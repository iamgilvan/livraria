package br.com.casadocodigo.livraria.produtos;

import br.com.casadocodigo.livraria.Autor;
import br.com.casadocodigo.livraria.exception.AutorNuloException;

//Classe abstrata
public abstract class Livro implements Object {
    private String nome;
    private String descricao;
    private double valor;
    private String isbn;
    private Autor autor;
    private boolean impresso;

    public void  mostraDetalhes(){
        String mensagem = "Mostrando detalhes do Livro ";
        System.out.println(mensagem);
        System.out.println("Nome :" +nome);
        System.out.println("Descrição :" +descricao);
        System.out.println("Valor :" + valor);
        System.out.println("ISBN: " + isbn);
    }
//Método abstrato
    //public abstract boolean aplicaDescontoDe (double porcentagem);

    public Livro(Autor autor) {

        if (autor == null){
            throw new AutorNuloException(
                    "O autor do livro não pode ser nulo");
        }
        this.isbn = "000-00-00000-00-0";
        this.autor = autor;
        this.impresso = true;
    }
    public Livro() {

    }
@Override
    public int compareTo(Produto outro){
        if (this.getValor() < outro.getValor()){
            return -1;
        }
        if (this.getValor() > outro.getValor()){
            return 1;
        }
        return 0;
    }
//métodos de acesso
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public boolean isImpresso() {
        return impresso;
    }

    public void setImpresso(boolean impresso) {
        this.impresso = impresso;
    }
}
