package br.com.casadocodigo.livraria.teste;

import br.com.casadocodigo.livraria.Autor;
import br.com.casadocodigo.livraria.produtos.Livro;
import br.com.casadocodigo.livraria.produtos.LivroFisico;


public class CadastroDeLivros {
    public static void main(String[] args) {

        Autor autor = new Autor();
        autor.setNome("Gilvan Almeida");
        autor.setEmail("g6almeida@aluno.com");
        autor.setCpf("111.111.111-11");

        Livro livro = new LivroFisico(autor);
        livro.setNome("Java Prático 8");
        livro.setDescricao("Novos Recursos da Linguagem");
        livro.setValor(59.90);
        livro.setIsbn("978-85-66250-46-6");
        livro.mostraDetalhes();
        autor.mostraDetalhes();

        Autor outroAutor = new Autor();
        outroAutor.setNome("Gilvan Almeida");
        outroAutor.setEmail("g6almeida@aluno.com");
        outroAutor.setCpf("222.222.222-22");

        Livro outroLivro = new LivroFisico(outroAutor);
        outroLivro.setNome("Lógica de Programação");
        outroLivro.setDescricao("Crie seus proprios programas");
        outroLivro.setValor(59.90);
        outroLivro.setIsbn("978-85-66250-46-6");
        outroLivro.mostraDetalhes();
        outroAutor.mostraDetalhes();

    }
}
